<?php

namespace Modules\Example\Listeners;

use App\Events\StoreCreditSaving as Event;
use App\Models\Role;

/**
 * This event listener grants users a role named 'Donator' if their credit balance becomes positive.
 */
class StoreCreditSaving
{
    public function handle(Event $event)
    {
        $role = Role::firstOrCreate(
            ['name' => 'Donator'],
            ['ingame_equivalent' => 'donator']
        );

        if ($event->storeCredit->credits ?? 0 > 0)
            $event->storeCredit->user->addRole($role->rid);
    }
}

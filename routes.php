<?php

use Modules\Example\Controllers\ExampleController;
use Slim\App;

return function (App $app) {
    $app->get('/example', ExampleController::class . ':example');
};

<?php

namespace Modules\Example\Providers;

use App\Providers\ModuleServiceProvider;

class ExampleServiceProvider extends ModuleServiceProvider
{
    protected $listen = [
        \App\Events\StoreCreditSaving::class => [\Modules\Example\Listeners\StoreCreditSaving::class],
    ];

    public function boot(): void
    {
        $this->loadRoutesFrom($this->module->getPath('routes.php'));
        $this->loadViewsFrom($this->module->getPath('resources/views'));

        $this->navbarLinks([
            [
                'icon' => 'fas fa-box',
                'name' => 'Example',
                'url' => '/example',
            ],
        ]);
    }
}

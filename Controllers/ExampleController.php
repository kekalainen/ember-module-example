<?php

namespace Modules\Example\Controllers;

use App\Controllers\Controller;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ExampleController extends Controller
{
    public function example(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $this->view->getEnvironment()->addGlobal('data', [
            'title' => 'Example module',
            'content' => 'This is an example module for <a href="https://www.gmodstore.com/market/view/5620">Ember</a>.'
        ]);

        return $this->view->render($response, 'example.twig');
    }
}
